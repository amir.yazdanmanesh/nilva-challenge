import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nilva_challenge/feature/auth/presentation/pages/sign_in_page.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../../../core/constanst/numbers/spacings.dart';
import '../../../../core/constanst/strings/language.dart';
import '../../../../core/utils/toasts.dart';
import '../../../../core/widgets/simple_widgets.dart';
import '../../domain/blocs/login/auth_cubit.dart';
import 'home_page.dart';

class OtpPage extends StatefulWidget {
  static const id = "OtpPage";

  const OtpPage({Key? key}) : super(key: key);

  @override
  State<OtpPage> createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  final int otpLength = 5;
  String? code;
  final pinCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final strings = Languages.of(context);
    final theme = Theme.of(context);
    final args = ModalRoute.of(context)!.settings.arguments as LoginArguments;

    return BlocConsumer<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is SubmitVerificationCodeCompleteState) {
          Navigator.of(context).pushNamed(HomePage.id);
        }
        if (state is SubmitVerificationCodeFailState) {
          showToast(state.errorMessage!, context, error: true);
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('سامانه اهدای خون'),
          ),
          body: Center(
            child: Container(
              width: double.infinity,
              height: double.infinity,
              margin: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  title(strings, args.phoneNumber),
                  padding(),
                  Directionality(
                    textDirection: TextDirection.ltr,
                    child: PinCodeTextField(
                      controller: pinCodeController,
                      length: otpLength,
                      autoFocus: true,
                      obscureText: false,
                      cursorWidth: 1,
                      keyboardType: TextInputType.number,
                      animationType: AnimationType.fade,
                      pinTheme: PinTheme(
                          shape: PinCodeFieldShape.box,
                          borderRadius: BorderRadius.circular(Spacings.size3Xs),
                          fieldHeight: Spacings.sizeSm,
                          fieldWidth: Spacings.sizeSm,
                          borderWidth: Spacings.picoSize,
                          inactiveColor: theme.colorScheme.onSurface,
                          activeColor: theme.colorScheme.primary,
                          selectedColor: theme.colorScheme.onSurface),
                      animationDuration: const Duration(milliseconds: 300),
                      onCompleted: (v) {
                        onCodeSubmit(args.requestId, code, context);
                      },
                      onChanged: (value) {
                        setState(() {
                          code = value;
                        });
                      },
                      beforeTextPaste: (text) {
                        return true;
                      },
                      appContext: context,
                    ),
                  ),
                  padding(),
                  button(strings, state, theme, args.requestId),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  button(Languages strings, AuthState state, ThemeData theme, String requestId) {
    return Padding(
        padding: const EdgeInsets.all(Spacings.marginLg),
        child: FilledButton(
            onPressed: (validation())
                ? () {
              onCodeSubmit(requestId, code, context);

            }
                : null,
            style: FilledButton.styleFrom(
              disabledBackgroundColor:
              theme.colorScheme.secondary.withOpacity(0.5),
              backgroundColor: theme.primaryColor,
              padding:  const EdgeInsets.all(Spacings.marginLg),
              shape: const RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.all(Radius.circular(Spacings.radiusXl))),
              minimumSize: const Size.fromHeight(Spacings.sizeXs),
            ),
            child: state is SubmitLoadingState
                ? CircularProgressIndicator(color: theme.colorScheme.onPrimary)
                : Text(strings.phoneNumberSubmitLabel)));
  }
  bool validation() {
    return (code?.length ?? 0) == 5;
  }
  padding() {
    return const SizedBox(height: Spacings.marginLg);
  }

  title(Languages strings, String? phoneNumber) {
    return InputLabel(
      title: strings.verificationCodeSubmitTitle,
      description:
          "${strings.verificationCodeDescription[0]} $phoneNumber ${strings.verificationCodeDescription[1]}",
    );
  }

  onCodeSubmit(requestId, code, BuildContext context) async {
    FocusManager.instance.primaryFocus?.unfocus();
    context
        .read<AuthCubit>()
        .submitCode(requestId, code);
  }

}
