import 'package:dio/dio.dart';
import 'package:nilva_challenge/feature/auth/data/model/login_request.dart';
import 'package:nilva_challenge/feature/auth/data/model/login_response.dart';
import 'package:nilva_challenge/feature/auth/data/model/submit_code_request.dart';
import 'package:nilva_challenge/feature/auth/data/model/submit_code_response.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';

import '../../../../../core/constanst/general.dart';

part 'login_service.g.dart';

@RestApi(baseUrl: GeneralConstants.kBaseUrl)
abstract class LoginService {
  factory LoginService(Dio dio, {String baseUrl}) = _LoginService;

  @POST('users/auth/request-otp?action=login')
  Future<HttpResponse<LoginResponse>> submitPhone(
      @Body() LoginRequest loginRequest);

  @POST("users/auth/get-token")
  Future<HttpResponse<SubmitCodeResponse>> submitCode(
      @Body() SubmitCodeRequest submitCodeRequest);
}
