// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginRequest _$LoginRequestFromJson(Map<String, dynamic> json) => LoginRequest(
      phoneNumber: json['phoneNumber'] as String?,
      nationalCode: json['nationalCode'] as String?,
      captcha: json['captcha'] == null
          ? null
          : Captcha.fromJson(json['captcha'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LoginRequestToJson(LoginRequest instance) =>
    <String, dynamic>{
      'phoneNumber': instance.phoneNumber,
      'nationalCode': instance.nationalCode,
      'captcha': instance.captcha,
    };
