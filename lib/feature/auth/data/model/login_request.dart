import 'package:json_annotation/json_annotation.dart';
import 'capcha.dart';
part 'login_request.g.dart';

@JsonSerializable()
class LoginRequest {
  String? phone_number;
  String? national_code;
  String? captcha;

  LoginRequest({this.phone_number, this.national_code, this.captcha});

  factory LoginRequest.fromJson(Map<String, dynamic> json) => _$LoginRequestFromJson(json);

  Map<String, dynamic> toJson() => _$LoginRequestToJson(this);
}
