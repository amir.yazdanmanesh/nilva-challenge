// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'capcha.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Captcha _$CaptchaFromJson(Map<String, dynamic> json) => Captcha(
      id: json['id'] as String,
      code: json['code'] as String,
    );

Map<String, dynamic> _$CaptchaToJson(Captcha instance) => <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
    };
