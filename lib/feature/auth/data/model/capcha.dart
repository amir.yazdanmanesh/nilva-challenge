import 'package:json_annotation/json_annotation.dart';
part 'capcha.g.dart';

@JsonSerializable()
class Captcha {
  String? id;
  String? code;

  Captcha({required this.id, required this.code});

  factory Captcha.fromJson(Map<String, dynamic> json) => _$CaptchaFromJson(json);

  Map<String, dynamic> toJson() => _$CaptchaToJson(this);
}
