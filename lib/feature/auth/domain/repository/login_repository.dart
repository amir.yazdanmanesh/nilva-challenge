import 'package:injectable/injectable.dart';
import 'package:nilva_challenge/feature/auth/data/model/login_response.dart';
import 'package:nilva_challenge/feature/auth/data/model/submit_code_request.dart';
import 'package:nilva_challenge/feature/auth/data/model/submit_code_response.dart';

import '../../../../core/utils/Wrapper.dart';
import '../../data/model/login_request.dart';

abstract class LoginRepository {
  Future<DataResult<LoginResponse>> submitPhone(LoginRequest loginRequest);
  Future<DataResult<SubmitCodeResponse>> submitCode(SubmitCodeRequest submitCodeRequest);
  Future setAuthToken(String authToken);
  Future<bool> checkUserIsLogin();
}