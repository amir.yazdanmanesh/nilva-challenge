import 'package:injectable/injectable.dart';
import 'package:nilva_challenge/feature/auth/domain/repository/login_repository.dart';

@injectable
class CheckUserIsLoginUseCase {
  final LoginRepository _loginRepository;

  CheckUserIsLoginUseCase(this._loginRepository);

  Future<bool> checkUserIsLogin() {
    return _loginRepository.checkUserIsLogin();
  }
}
