import 'package:injectable/injectable.dart';
import 'package:nilva_challenge/feature/auth/data/model/login_request.dart';
import 'package:nilva_challenge/feature/auth/data/model/login_response.dart';
import 'package:nilva_challenge/feature/auth/domain/repository/login_repository.dart';

import '../../../../../core/utils/Wrapper.dart';

@injectable
class SubmitNumberUseCase {
  final LoginRepository _loginRepository;

  SubmitNumberUseCase(this._loginRepository);

  Future<DataResult<LoginResponse>> submitNumber(LoginRequest loginRequest) {
    return _loginRepository.submitPhone(loginRequest);
  }
}
