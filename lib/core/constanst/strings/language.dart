import 'package:flutter/cupertino.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

part 'language_fa.dart';

class AppLocalizationsDelegate extends LocalizationsDelegate<Languages> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['fa'].contains(locale.languageCode);

  @override
  Future<Languages> load(Locale locale) => _load(locale);

  static Future<Languages> _load(Locale locale) async {
    switch (locale.languageCode) {
      case 'fa':
        return LanguageFa();
      default:
        return LanguageFa();
    }
  }

  @override
  bool shouldReload(LocalizationsDelegate<Languages> old) => false;
}

abstract class Languages {
  static final Languages defaultLanguage = LanguageFa();

  static Languages of(BuildContext? context) {
    if (context != null) {
      return Localizations.of<Languages>(context, Languages)!;
    }
    return defaultLanguage;
  }

  static const LocalizationsDelegate<Languages> delegate =
      AppLocalizationsDelegate();

  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates =
      <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[Locale('fa')];

  // ------------- general
  String get appName;

  String get searchCenter;

  String get searchProvince;

  String get searchCity;

  String get centersList;

  String get centersOnMap;

  String get savedCenters;

  String get donationDate;

  String get donationTime;

  String get resume;

  String get splashDescription;

  String get poweredByLabel;

  String get title;

  String get notices;

  String get comingSoon;

  String get nothingFound;

  String get thisFieldIsMandatory;

  String get theNationalCodeIsInvalid;

  String get theBirthCertificateNumberIsInvalid;

  String get theMobileNumberIsInvalid;

  // ------------- intro
  List<String> get introTitles;

  List<String> get introDescriptions;

  String get introNextButtonLabel;

  // ------------- get phone number
  String get phoneNumberHintText;

  String get phoneNumberInputLabel;

  String get logIn;

  String get phoneNumberInputDescription;

  String get phoneNumberSubmitLabel;

  String get loginWithSSO;

  List<String> get doYouHaveAnAccount;

  // ------------- get register
  String get register;

  String get name;

  String get lastName;

  String get fatherName;

  String get birthCertificateNumber;

  String get dateOfBirth;

  String get phoneNumber;

  String get province;

  String get city;

  String get residenceAddress;

  String get postalCode;

  String get all;

  String get termsAndConditions;

  String get iHaveReadAndAcceptThem;

  String get haveAccount;

  // ------------- get home
  String get charity;

  String get bloodDonation;

  String get bloodDonationCard;

  String get healthFile;

  String get records;

  String get gym;

  String get more;

  // ------------- get verification code
  String get verificationCodeInputLabel;

  String get verificationCodeSubmitLabel;

  List<String> get resendWaitingVerificationCodeLabel;

  String get untilResendVerificationCodeLabel;

  String get resendVerificationCodeLabel;

  List<String> get verificationCodeDescription;

  String get verificationCodeSubmitTitle;

  String get changePhoneNumberLabel;

  String get fatherLabel;

  String get naturalCode;

  String get motherLabel;

  String get appWebsite;

  String get home;

  String get family;

  String get devices;

  String get setting;

  String get welcome;

  String get intro;

  String get start;

  String get accept;

  String get cancel;

  String get yes;

  String get no;

  String get understand;

  String get forgotPassMessage;

  String get passSetTitle;

  String get passSetRepeatTitle;

  String get enterPassTitle;

  String get firstPassSetDescription;

  String get firstPassSetRepeatDescription;

  String get enterPassDescription;

  String get gender;

  String get boy;

  String get girl;

  String get birthDate;

  String get selectMessage;

  String get update;

  String get support;

  String get deleteFamily;

  String get deleteFamilyMessage;

  String get noUpdateMessage;

  String get deleteFamilyQuestion;

  String get deleteFamilyWarning;

  String get updateDialogTitle;

  String get updateDialogDescription;

  String get protectionLevel;

  String get familyInternet;

  String get functionalReport;

  String get parents;

  String get children;

  String get changeUser;

  String get noErrorMessage;

  String get addChild;

  String get version;

  String get permissionsState;

  String get currentUser;

  String get usageStatsTitle;

  String get usageStatsDescription;

  String get usageStatsGrant1;

  String get usageStatsGrant2;

  String get phoneAccessDescription;

  String get phoneAccessGrant1;

  String get phoneAccessGrant2;

  String get phoneAccessTitle;

  String get change;

  String get grantPermissionMessage;

  String get grantPermissions;

  // ------------- reservation
  String get calendarTitle;

  String get loadingReservation;

  // ------------- preview login
  String get previewLoginTitles;

  String get previewLoginDescriptions;

  // ------------- failure messages
  String get fetchDataFailureMessage;

  String get accessDeniedFailureMessage;

  String get connectionFailureMessage;

  String get captchaCode;

  String get invalidCaptchaCode;

  String get requiredField;

  String get nameAndLstName;

  String get dateTime;

  String get centerAddress;

  String get address;

  String get bloodDonationCenter;

  String get donorInformation;

  String get appointmentReservation;

  String get reservationInformation;

  String get reservationCode;

  String get receiveAppointmentReceipt;

  String get successfulReservation;

  String get failReservation;

  String get retry;
}
