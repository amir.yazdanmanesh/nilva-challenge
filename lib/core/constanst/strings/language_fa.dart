part of 'language.dart';

class LanguageFa extends Languages {
  // ------------- general

  @override
  String get appName => "اهدای خون";

  @override
  String get searchCenter => "جستجو مرکز";

  @override
  String get searchProvince => "جستجو استان";

  @override
  String get searchCity => "جستجو شهر";

  @override
  String get resume => "ادامه";

  @override
  String get splashDescription =>
      "با مشارکت و همکاری شرکت ارتباطات سیار (همراه اول)";

  @override
  String get version => "نسخه";

  @override
  String get poweredByLabel => "قدرت گرفته از همراه اول";

  @override
  String get appWebsite => "https://app_donation_flutter.com";

  @override
  String get title => 'پیش ثبت نام دوازدهمین دوره مجلس شورای اسلامی';

  @override
  String get notices => 'اطلاع رسانی';

  @override
  String get comingSoon => 'به زودی اضافه می‌گردد';

  @override
  String get nothingFound => 'چیزی یافت نشد';

  @override
  String get thisFieldIsMandatory => 'این فیلد اجباری است';

  @override
  String get theNationalCodeIsInvalid => 'شماره ملی نامعتبر است';

  @override
  String get theBirthCertificateNumberIsInvalid => 'شماره شناسنامه نامعتبر است';

  @override
  String get theMobileNumberIsInvalid => 'شماره همراه نامعتبر است';

  // ------------- get intro
  @override
  List<String> get introTitles =>
      ["پرونده سلامت", "نوبت دهی آنلاین اهدای خون", "باشگاه اهدا کنندگان"];

  @override
  List<String> get introDescriptions => [
        "شما می‌توانید به اطلاعات سلامت خود دسترسی داشته باشید",
        "شما می‌توانید با اهدای خون به دیگران کمک کنید",
        "با اهدای خون از جوایز و تخفیفات ویژه برخوردار شوید"
      ];

  @override
  String get introNextButtonLabel => "بعدی";

  // ------------- get phone number
  @override
  String get phoneNumberHintText => "مثلا 09123456789";

  @override
  String get phoneNumberInputLabel => "شماره همراه";

  @override
  String get phoneNumberInputDescription => "ورود به سیستم";

  @override
  String get phoneNumberSubmitLabel => "ورود";

  @override
  String get logIn => "ورود";

  @override
  List<String> get doYouHaveAnAccount => ["حساب کاربری ندارید؟  ", "ثبت‌نام"];

  // ------------- get verification code
  @override
  String get changePhoneNumberLabel => "";

  @override
  String get untilResendVerificationCodeLabel => "تا ارسال مجدد کد";

  @override
  String get resendVerificationCodeLabel => "ارسال مجدد کد";

  @override
  List<String> get resendWaitingVerificationCodeLabel =>
      ["ارسال مجدد کد تا", "دیگر"];

  @override
  String get verificationCodeSubmitLabel => "ادامه";

  @override
  String get verificationCodeSubmitTitle => "کد احراز هویت";

  @override
  List<String> get verificationCodeDescription =>
      ["کد احراز هویت برای شماره", "ارسال گردید."];

  @override
  String get loginWithSSO => "ورود از طریق پنجره‌ی خدمات وزارت کشور";

  // ------------- get register
  @override
  String get register => "ثبت نام";

  @override
  String get name => "نام";

  @override
  String get lastName => "نام خانوادگی";

  @override
  String get fatherName => "نام پدر";

  @override
  String get birthCertificateNumber => "شماره شناسنامه";

  @override
  String get dateOfBirth => "تاریخ تولد";

  @override
  String get phoneNumber => "تلفن همراه";

  @override
  String get province => "استان";

  @override
  String get city => "شهر";

  @override
  String get residenceAddress => "آدرس محل سکونت";

  @override
  String get postalCode => "کد پستی";

  @override
  String get all => "کلیه";

  @override
  String get termsAndConditions => "قوانین و مقررات";

  @override
  String get iHaveReadAndAcceptThem => "را خوانده ام و آنها را می پذیرم.";

  @override
  String get haveAccount => "حساب کاربری دارید؟";

  // ------------- get home
  @override
  String get charity => 'نیکوکاری';

  @override
  String get bloodDonation => 'اهدای خون';

  @override
  String get bloodDonationCard => 'کارت اهدای خون';

  @override
  String get healthFile => 'پرونده سلامت';

  @override
  String get records => "سوابق";

  @override
  String get gym => "باشگاه";

  @override
  String get more => "بیشتر";

  // ------------- family related
  @override
  String get naturalCode => "کد ملی";

  @override
  String get fatherLabel => "پدر";

  @override
  String get motherLabel => "مادر";

  @override
  String get girl => "دختر";

  @override
  String get boy => "پسر";

  @override
  String get family => "خانواده";

  @override
  String get parents => "والدین";

  @override
  String get accept => "تایید";

  @override
  String get addChild => "افزودن فرزند";

  @override
  String get birthDate => "تاریخ تولد";

  @override
  String get cancel => "لغو";

  @override
  String get changeUser => "تغییر کاربر";

  @override
  String get children => "فرزندان";

  @override
  String get currentUser => "کاربر جاری";

  @override
  String get deleteFamily => "حذف خانواده";

  @override
  String get deleteFamilyMessage => "حذف اعضاء و کلیه اطلاعات آنها";

  @override
  String get deleteFamilyQuestion => "خانواده را حذف می‌کنید؟";

  @override
  String get deleteFamilyWarning =>
      "در صورتی که خانواده را حذف کنید، کلیه افراد و اطلاعات مربوط به آنها از سامانه حذف خواهد شد و دیگر امکان بازیابی وجود ندارد";

  @override
  String get devices => "دستگاه‌ها";

  @override
  String get enterPassDescription => "برای ورود گذرواژه خود را وارد کنید";

  @override
  String get enterPassTitle => "ورود به عنوان سرپرست";

  @override
  String get familyInternet => "اینترنت خانواده";

  @override
  String get firstPassSetDescription =>
      "این رمز برای نرم‌افزار تعیین می‌شود، برای امنیت لازم است هر بار در هنگام ورود به نرم‌افزار این رمز را وارد نمایید";

  @override
  String get firstPassSetRepeatDescription =>
      "برای اطمینان از عدم فراموشی، رمز عبور انتخاب شده در مرحله قبل را مجددا وارد نمایید";

  @override
  String get forgotPassMessage => "گذرواژه خود را فراموش کردم";

  @override
  String get functionalReport => "گزارش عملکرد";

  @override
  String get gender => "جنسیت";

  @override
  String get home => "خانه";

  @override
  String get intro =>
      "پادتن به شما برای محافظت از فرزندان‌تان در فضای مجازی و همچنین مدیریت هوشمند زمان استفاده از تلفن همراه کمک می‌کند.";

  @override
  String get no => "خیر";

  @override
  String get noErrorMessage => "خطایی وجود ندارد";

  @override
  String get noUpdateMessage => "بروزرسانی وجود ندارد";

  @override
  String get passSetRepeatTitle => "تکرار رمز عبور";

  @override
  String get passSetTitle => "انتخاب رمز عبور";

  @override
  String get permissionsState => "وضعیت دسترسی‌ها";

  @override
  String get protectionLevel => "وضعیت محافظت";

  @override
  String get selectMessage => "انتخاب کنید";

  @override
  String get setting => "تنظیمات";

  @override
  String get start => "شروع کن";

  @override
  String get support => "پشتیبانی";

  @override
  String get understand => "متوجه شدم";

  @override
  String get update => "بروزرسانی";

  @override
  String get updateDialogDescription =>
      "در صورتی که خانواده را حذف کنید، کلیه افراد و اطلاعات مربوط به آنها از سامانه حذف خواهد شد و دیگر امکان بازیابی وجود ندارد";

  @override
  String get updateDialogTitle => "نرم‌افزار با موفقیت بروزرسانی شد";

  @override
  String get welcome => "به پادتن خوش آمدید";

  @override
  String get yes => "بله";

  @override
  String get change => "تغییر";

  @override
  String get grantPermissionMessage => "برای ایجاد دسترسی";

  @override
  String get grantPermissions => "اعطای دسترسی‌ها";

  @override
  String get phoneAccessDescription =>
      "برای فعال‌سازی اینترنت خانواده پادتن به دسترسی تلفن نیاز دارد.";

  @override
  String get phoneAccessGrant1 => "بر روی دکمه تغییر کلیک کنید";

  @override
  String get phoneAccessGrant2 => "در پنجره باز شده بر روی Allow کلیک کنید";

  @override
  String get phoneAccessTitle => "دسترسی به تلفن";

  @override
  String get usageStatsDescription =>
      "چاپگرها و متون بلکه روزنامه و مجله در ستون و سطر آنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می‌باشد";

  @override
  String get usageStatsGrant1 =>
      "در صفحه بعد نرم ازفزار پادتن را انتخاب کنید و بعد از انتخاب آن وارد صفحه جدید خواهید شد";

  @override
  String get usageStatsGrant2 => "در این صفحه گزینه مقابل setting را فعال کنید";

  @override
  String get usageStatsTitle => "دسترسی به آمار استفاده از دستگاه";

  // ------------- reservation
  @override
  String get calendarTitle => "تاریخ اهدا";

  @override
  String get loadingReservation => "در حال رزرو نوبت لطفا منتظر بمانید...";

  // ------------- preview login
  @override
  String get previewLoginTitles => "لطفا وارد شوید";

  @override
  String get previewLoginDescriptions =>
      "برای دسترسی به نوبت دهی اهدا خون لطفا وارد حساب کاربری خود شوید";

  // ------------- failure messages
  @override
  String get fetchDataFailureMessage => "مشکل در برقراری ارتباط";

  @override
  String get connectionFailureMessage => "";

  @override
  String get accessDeniedFailureMessage => throw UnimplementedError();

  @override
  String get verificationCodeInputLabel => "کد تایید";

  @override
  String get centersList => "لیست مراکز";

  @override
  String get centersOnMap => "مراکز روی نقشه";

  @override
  String get savedCenters => "مراکز ذخیره شده";

  @override
  String get donationDate => "تاریخ اهدا";

  @override
  String get donationTime => "زمان اهدا";

  @override
  String get captchaCode => "کد امنیتی";

  @override
  String get invalidCaptchaCode => "کد امنیتی معتبر نیست";

  @override
  String get requiredField => "این فیلد اجباری است";

  @override
  String get appointmentReservation => "رزرو نوبت";

  @override
  String get nameAndLstName => "نام و نام خانوادگی";

  @override
  String get dateTime => "تاریخ و ساعت";

  @override
  String get centerAddress => "آدرس محل اهدا";

  @override
  String get address => "آدرس";

  @override
  String get bloodDonationCenter => "مرکز اهدای خون ";

  @override
  String get donorInformation => "اطلاعات اهدا کننده";

  @override
  String get reservationInformation => "اطلاعات رزرو";

  @override
  String get reservationCode => "کد رزرو";

  @override
  String get receiveAppointmentReceipt => "دریافت رسید نوبت";

  @override
  String get failReservation => "رزرو نوبت شما انجام نشد!";

  @override
  String get successfulReservation => "نوبت شما با موفقیت رزرو شد";

  @override
  String get retry => "تلاش مجدد";
}
